tinymsg
=======

Tiny messages system for communicating threads each other.

Usage
-----

```
scheme@(guile-user)> (use-modules (tinymsg))
scheme@(guile-user)> (send-message 'msgbox "Hello World!!")
$1 = "Hello World!!"
scheme@(guile-user)> (receive-message 'msgbox)
$2 = "Hello World!!"
scheme@(guile-user)> (map
                       (lambda (n)
                         (send-message 'msgbox n))
                       '(1 2 3 4 5))
$3 = (1 2 3 4 5)
scheme@(guile-user)> (flush-messages 'msgbox)
msgbox got 1
msgbox got 2
msgbox got 3
msgbox got 4
msgbox got 5
```

Requirements
------------

* GNU Guile >= 2.0.11

License
-------

GNU GPLv3+
